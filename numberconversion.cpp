#include "numberconversion.h"
using namespace std;

int value (char roman){
  switch (roman)
    {
    case 'I':return 1;
    case 'V':return 5;
    case 'X':return 10;
    case 'L':return 50;
    case 'C':return 100;
    case 'D':return 500;
    case 'M':return 1000;
    }
}

int romantoint(std::string roman_numeral)
{
  int i, n, ans=0, p=0;
  n=roman_numeral.length()-1;

  for (i = n; i>= 0; i--){
    if ( value(roman_numeral[i]) >= p) {
      ans = ans +value(roman_numeral[i]);}
    else
      {ans = ans - value(roman_numeral[i]);}

    p = value(roman_numeral[i]);
    
  }
  
  return ans;
}

std::string inttoroman(int number)
{
    int num[] = {1,4,5,9,10,40,50,90,100,400,500,900,1000};
    string sym[] = {"I","IV","V","IX","X","XL","L","XC","C","CD","D","CM","M"};
    int i=12;    
    while(number>0)
    {
      int div = number/num[i];
      number = number%num[i];
      while(div--)
      {
       return  sym[i];
      }
      i--;
    }
}
